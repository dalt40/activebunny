require_relative 'basics'
require_relative 'active_model/hooks'

module ActiveBunny
  class Subscriber
    extend ActiveSupport::DescendantsTracker
    extend ::ActiveBunny::Basics

    attr_accessor :nack
    attr_accessor :drop

    def initialize
      self.nack = false
      self.drop = false
    end

    def drop
      self.nack = true
      self.drop = true
    end

    def failure
      self.nack = true
    end
    alias error failure
    alias nack failure

    def ack?
      !nack
    end

    def requeue?
      !drop
    end
  end
end